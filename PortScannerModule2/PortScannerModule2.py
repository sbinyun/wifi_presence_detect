import time
import copy
import uuid
import nmap
from threading import Thread

def get_fake_devices(count):
	print("Generating fake devices")

	devices = []
	for i in range(0, count):
		devices.append(get_fake_device())

	return devices

def get_fake_device(ip_address = None, mac = None,online_status = None):
	from faker import Faker
	fake = Faker()
	if ip_address is None:
		ip_address = fake.ipv4()

	if mac is None:
		mac = fake.mac_address()


	device = Device(ip_address, mac, name_dictionary[mac])

	if online_status is not None:
		device.online = online_status

	return device


def generate_fake_status(devices):
	from random import randint
	for device in devices:
		device.online = True if randint(0,1) else False

	return devices

name_dictionary = {"00:14:D1:B1:F7:A8": "Ray's Redmi",
					"00:25:4b:90:af:c8": "Apple Macbook",
			"64:09:80:DD:71:A8": "Xiaomi Mi 3",
			"1C:56:FE:CE:C2:3B": "Ray's Nexus 6",
			"64:CC:2E:A1:40:67": "Xiaomi Mi Note 3",
			"30:74:96:71:31:A8": "Huawei Modem",
			"b3:8a:b6:01:fc:db": "Najib's iPhone",
			"42:bd:90:e6:eb:88": "Rosmah's MacBook",
			"6e:dd:df:2f:1a:c1": "Ray's Surface Pro",
			"23:6a:54:f0:d8:d1": "BT's Kabylake Machine"}

#authorized_devices = {"74:DF:BF:AB:4E:F4": "Dad's phone",
#                      "64:09:80:DD:71:A8": "Xiaomi Mi 3"}

#authorized_devices = ["mac":"74:DF:BF:AB:4E:F4"],
#                     ["mac":"64:09:80:DD:71:A8"] 

#controlled_devices = {"6e:dd:df:2f:1a:c1": "Ray's Surface Pro"}


class PortScanner(Thread):
	def __init__(self,subnet_mask, ip_address, device_queue_1, device_queue_2, refresh_rate = 10, emulation = False):
		print("[INFO] Listening to device(s) on network.")
		Thread.__init__(self)
		self.refresh_rate = refresh_rate
		self.device_queue_1 = device_queue_1
		self.device_queue_2 = device_queue_2
		self.emulation = emulation
		if not self.emulation: self.nmap_scanner = NmapScanner(subnet_mask, ip_address)
		self.past_devices = []
		self.device_deltas = []
		self.checked_devices = []
		self.authorized_devices = []
		self.controlled_devices = []


	def run(self):
		time.sleep(1)
		if self.emulation:
			self.current_devices = get_fake_devices(10)
		else:
			self.current_devices = self.nmap_scanner.get_devices()

		i = 0
		while(1):
			print("[REFRESH] Getting new device(s) list.")
			self.past_devices = copy.deepcopy(self.current_devices)

			# Emulation mode
			if self.emulation:
				if (i % 5) == 0:
					self.current_devices.append(get_fake_device())

				if (i % 8) == 0:
					self.current_devices = self.current_devices[:-2]

				self.current_devices = generate_fake_status(self.current_devices)
				self.current_devices.append(get_fake_device())

			else:
				self.current_devices = self.nmap_scanner.get_devices() 

			self.device_deltas = self.get_devices_delta()  
			
#			self.authorized_devices = self.get_authorized_devices()
#			self.controlled_devices = self.get_controlled_devices()
			self.checked_devices = self.check_device_approval()

			payload = self.generate_payload(self.current_devices, self.device_deltas, self.checked_devices)

			self.device_queue_1.put(payload)
			self.device_queue_2.put(payload) # send to mqtt

#			for keys, values in self.checked_devices.results():
#				print(keys)
#				print(values)

			print(self.checked_devices)

			time.sleep(self.refresh_rate)
			i+=1

	def get_authorized_devices(self):
		authorized_devices =[]
		mac = "74:DF:BF:AB:4E:F4"
		hostname = "Dad's phone"
		authorized_devices.append(authorized_device(mac,hostname))
		return authorized_devices

	def get_controlled_devices(self):
		controlled_devices =[]
		mac = "64:09:80:DD:71:A8"
		hostname = "Xiaomi Phone"
		controlled_devices.append(controlled_device(mac,hostname))
		return controlled_devices

	def check_device_approval(self):
		authorized_devices_mac = [authorized_device.mac for authorized_device in self.authorized_devices]
		controlled_devices_mac = [controlled_device.mac for controlled_device in self.controlled_devices]
#		authorized_devices_mac = ["64:09:80:DD:71:A8"]
#		controlled_devices_mac = controlled_devices.mac
#		controlled_devices_mac = ["74:DF:BF:AB:4E:F4"]  #iphone mac address
		current_devices_mac = [device.mac for device in self.current_devices]

		print("check device mac: ",current_devices_mac)

		current_approved_devices_mac = list(set(current_devices_mac) & set(authorized_devices_mac))

		current_controlled_devices_mac = list(set(current_devices_mac) & set(controlled_devices_mac))

		current_unauthorized_devices_mac = list(set(current_devices_mac) - set(authorized_devices_mac))

		results = {"current_approved_devices_mac": current_approved_devices_mac,
				   "current_controlled_devices_mac": controlled_devices_mac,
				   "current_unauthorized_devices_mac": current_unauthorized_devices_mac}

		return results

	def get_devices_delta(self):

		added_devices = []
		dropped_devices = []
		status_changed_devices = []

		current_devices_ip = [device.ip for device in self.current_devices]
		past_devices_ip = [device.ip for device in self.past_devices]

		added_devices_ip = list(set(current_devices_ip) - set(past_devices_ip))
		if added_devices_ip:
			added_devices = [copy.deepcopy(self.current_devices[self.get_index_of(self.current_devices, device)]) for device in added_devices_ip]

		dropped_devices_ip = list(set(past_devices_ip) - set(current_devices_ip))
		if dropped_devices_ip:
			dropped_devices = [copy.deepcopy(self.past_devices[self.get_index_of(self.past_devices,device)]) for device in dropped_devices_ip]


		mutual_devices_ip = list(set(past_devices_ip) & set(current_devices_ip))

		for device in mutual_devices_ip:
			status_1 = self.current_devices[self.get_index_of(self.current_devices, device)].online
			status_2 = self.past_devices[self.get_index_of(self.past_devices,device)].online 

			if status_1 != status_2 :
				status_changed_devices.append(copy.deepcopy(self.current_devices[self.get_index_of(self.current_devices,device)])) 

		packet_uuid = str(uuid.uuid4())

		delta = {
				"added_devices": added_devices,
				"dropped_devices": dropped_devices,
				"status_changes": status_changed_devices,
				"id": packet_uuid
				}
		
		return delta

	def get_index_of(self, devices, key):
		for index, device in enumerate(devices):
			if device.ip == key:
				return index

		return -1

	def generate_payload(self, current_devices, delta, checked_devices):
		payload = {"devices": current_devices,
					"delta": delta,
					"checked_devices": checked_devices
				}

		return payload

class NmapScanner():
	def __init__(self, subnet_mask, ip_address):
		self.subnet_mask = subnet_mask
		self.ip_address = ip_address
		print("[INFO] Listening to device(s) on network.")

	def get_devices(self):
		print("[NMAP Scanner] Scanning for devices.")
		ip_address = self.ip_address + "/24"
		devices =[]

		nm = nmap.PortScanner()
		nm.scan(ip_address, arguments='-sP')
		
		for host in nm.all_hosts():
			print(host)
			print ("STATUS:", nm[host]['status']['state'])
			#Print the MAC address
			try:
				ipv4 = nm[host]['addresses']['ipv4']
				mac = nm[host]['addresses']['mac']

				try:
					hostname = name_dictionary[mac]
				except KeyError:
					hostname = "unknownUser"

				print("IP Addresses: ",ipv4)
				print ("MAC ADDRESS:", mac)
				print("Hostname: ", hostname)
				devices.append(Device(ipv4,mac,hostname))
			
			except:
				mac = 'unknown'

		#Append some additional devices:sss
#		devices.append(get_fake_device("192.168.8.150","b3:8a:b6:01:fc:db", True))
#		devices.append(get_fake_device("192.168.8.165","42:bd:90:e6:eb:88", False))
#		devices.append(get_fake_device("192.168.8.166","6e:dd:df:2f:1a:c1", True))
#		devices.append(get_fake_device("192.168.8.170","23:6a:54:f0:d8:d1", False))

		del nm
		return devices

class Device():
	def __init__(self, ip_addr, mac_addr, hostname):
		self.ip = ip_addr
		self.mac = mac_addr
		self.hostname = hostname
		self.num_detected_offline = 0
		self.online = True

	def serialize(self):
		return {
			"ip": self.ip,
			"mac": self.mac,
			"hostname": self.hostname,
			"num_detected_offline": self.num_detected_offline,
			"online":self.online
		}

class authorized_device():
	def __init__(self, mac_addr, hostname):
		self.mac = mac_addr
		self.hostname = hostname

	def serialize(self):
		return {
			"mac": self.mac,
			"hostname": self.hostname
		}

class controlled_device():
	def __init__(self, mac_addr, hostname):
		self.mac = mac_addr
		self.hostname = hostname

	def serialize(self):
		return {
			"mac": self.mac,
			"hostname": self.hostname
		}
