var interval_1 = setInterval(function() {loadCurrentDevices()}, 2000);
var interval_2 = setInterval(function() {loadDeltas()}, 1000);

var domain = "https://323c3d57.ngrok.io";
var id;

Notification.requestPermission(function (permission) {
	if (permission === "granted") {
		console.log("Permission granted for Notifications.");
	}
});



function spawnNotification(body, icon, title) {
	var options = {
		body: body,
		icon: icon
	};

	var n = new Notification(title, options);
	setTimeout(n.close.bind(n), 2000);

}

function constructMessage(deviceIp, onlineStatus) {
		var body = String(deviceIp);
		body += " is now ";

		(onlineStatus == true)? body+="online!" : body+="offline!";

		return body
}

function loadDeltas() {
	var xHttpDelta = new XMLHttpRequest();
	
	xHttpDelta.onreadystatechange = function () {
		var obj = JSON.parse(this.response);

		// Check if ID is defined before. Aka, check for if this is the first load
		if (!(id === undefined)) {
			// Check if ID match
			if (id === obj["data"]["id"]) {
				// Old data, ignore this request.
				return
			}
		}
		
		id = obj["data"]["id"];
		var addedDevices = obj["data"]["added_devices"];
		var droppedDevices = obj["data"]["dropped_devices"];
		var statusChanges = obj["data"]["status_changes"];

		for (var i = 0; i < addedDevices.length; i++) {
				if (!(addedDevices[i] in statusChanges)) {
					spawnNotification(constructMessage(addedDevices[i].ip, true), "", "Wifi Presence Detect");
				}
		}

		for (var i = 0; i < droppedDevices.length; i++) {
				if (!(droppedDevices[i] in statusChanges)) {
					spawnNotification(constructMessage(droppedDevices[i].ip, false), "", "Wifi Presence Detect");
				}
		}


		for (var i = 0; i < statusChanges.length; i++) {
			spawnNotification(constructMessage(statusChanges[i].ip, statusChanges[i].online), "", "Wifi Presence Detect");
		}



	}

	xHttpDelta.open("GET", domain + "/delta", true);
	xHttpDelta.send();
}

function loadCurrentDevices() {
	var xHttpCurrentDevices = new XMLHttpRequest();

	xHttpCurrentDevices.onreadystatechange = function () {
		var obj = JSON.parse(this.response);
		var devices = obj["data"]["current_devices"];

		if (devices.length == 0) {
			// console.log("No new devices.");
			return
		}

		var table = document.getElementById("content_table");
		var numTableRows = table.rows.length;
		var startPosition = 1;

		for (var i = numTableRows - 1; i >= startPosition; i--) {
			table.deleteRow(i);
		}

		for (var i = 0; i < devices.length; i++) {
			console.log("[" + i + "] " + devices[i].hostname);
		}

		for (var i = 0; i < devices.length; i++) {
			var row = table.insertRow(startPosition++); 

			var ipAddressCell = row.insertCell(0);
			var macAddressCell = row.insertCell(1);
			var hostnameCell = row.insertCell(2);
			var statusCell = row.insertCell(3);

			ipAddressCell.innerHTML = devices[i].ip;
			macAddressCell.innerHTML = devices[i].mac;
			hostnameCell.innerHTML = devices[i].hostname;
			statusCell.innerHTML = ((devices[i].online == true)? "Online": "Offline");
		}

	}

	xHttpCurrentDevices.open("GET", domain + "/current_devices", true);
	xHttpCurrentDevices.send();
	
}