import paho.mqtt.client as mqtt
from threading import Thread
import os
import time



# The callback for when the client receives a CONNACK response from the server.
class MqttService(Thread):
	def __init__(self, config, device_queue):
		print("starting MqttService")
		Thread.__init__(self)
		self.client = mqtt.Client()
		self.mqtt_server = config
		self.device_queue = device_queue
		self.refresh_rate = 10
		run=self.run()

	def on_connect(self, client, userdata, flags, rc):
		print("Connected with result code "+str(rc))

		# Subscribing in on_connect() means that if we lose the connection and
		# reconnect then subscriptions will be renewed.
		#client.subscribe("$SYS/#")
		self.client.subscribe("esp/obd2/speed")
		self.client.subscribe("esp/obd2/rpm")

		# The callback for when a PUBLISH message is received from the server.
	def on_message(self, client, userdata, msg):
		print(msg.topic+" "+str(msg.payload))
		if(msg.topic == "esp/obd2/speed"):
			print("In speed topic")
			speed = int(msg.payload)
			if(speed > 90):
				print("Alert Parents. Sending speed "+str(msg.payload)+ "km/h")

	def run(self):

		time.sleep(1)
#		i=0
#		while(1):

		print("Running MQTT services")
#		self.client = mqtt.Client()
		self.client.on_connect = self.on_connect
		self.client.on_message = self.on_message
		print("mqtt server: " + self.mqtt_server)
		self.client.connect(self.mqtt_server,1883,60)


			# Blocking call that processes network traffic, dispatches callbacks and
			# handles reconnecting.
			# Other loop*() functions are available that give a threaded interface and a
			# manual interface.
		self.client.loop_forever()
		time.sleep(self.refresh_rate)
#			i+=1

	def get_payload(self):
		while(True):
			payload = self.device_queue.get()

			if payload is None:
				print("[PortScanner] Sleeping...")
				time.sleep(1)

			else:
				self.current_devices = payload["devices"]
				self.delta = payload["delta"]
				self.checked_devices = payload["checked_devices"]
				#self.handle_delta_payload()

#				i = 1
#				for device in self.current_devices:
#					if self.config["verbose"]: print("[CHATBOT] [{:2}] {:15}\t{:17}\t{:10}\t{}".format(i, device.ip, device.mac, device.hostname, device.online))
#					i += 1