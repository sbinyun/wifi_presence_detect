import time
import queue
import os
import threading
import json
from PortScannerModule2.PortScannerModule2 import PortScanner
from ChatBotModule.ChatBotModule import ChatBot
from flask import Flask, render_template, request, jsonify, make_response
from MqttModule.pahomqtt import MqttService

config = json.load(open("carconfig.json"))

webui_module_folder_path = os.path.join(os.getcwd(), config["webui_folder"])
template_folder_path = os.path.join(webui_module_folder_path, "templates")
static_folder_path = os.path.join(webui_module_folder_path, "static")

#app = Flask (config["app_name"], template_folder = template_folder_path, static_folder = static_folder_path)
current_devices = []
delta = []

###################################################
if config["proxy"]:
	import os
	os.environ["http_proxy"] = config["proxy"]
	os.environ["HTTP_PROXY"] = config["proxy"]
	os.environ["https_proxy"] = config["proxy"]
	os.environ["HTTP_PROXY"] = config["proxy"]

#################################################




def is_empty(delta):
	return not (delta["added_devices"] or delta["dropped_devices"] or delta["status_changes"])



if __name__ == "__main__":
	chatbot_queue = queue.Queue()
	mqtt_queue = queue.Queue()

	# Starting PortScanner module
	ps = PortScanner(config["subnet"], config["ip_address"], chatbot_queue, mqtt_queue, config["refresh_rate"], config["emulation_mode"])
	ps.daemon = True
	ps.start()

	# Starting Mqtt module
	cb = MqttService(config["mqtt_server"], mqtt_queue)
#	cb.daemon = True
#	cb.start()


	# Should not reach
	while (True):
		print("WPD is running.")
		time.sleep(10)