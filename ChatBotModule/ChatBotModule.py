import telepot
import time
import os
import sqlite3
from threading import Thread

class ChatBot():
	def __init__(self, config, device_queue):
		self.config = config
		self.device_queue = device_queue

		self.current_devices = []
		self.past_devices = []
		self.delta = []
		self.last_delta_uuid = None

		self.chat_bot = telepot.Bot(config["token"])
		self.chat_bot.setWebhook(config["domain"] + "/{}".format(self.config["token"]) + "/updates")

		self.pending_list = []
		self.active_users = []

		self.commands = { 
			"/status" : self.process_status_command,
			"/online" : self.process_online_command,
			"/offline": self.process_offline_command
		}

		listener_thread = Thread(target = self.get_payload)
		listener_thread.daemon = True
		listener_thread.start()
		
		print("[CHATBOT] Retrieving user id(s).")
		if not self.get_chat_ids(): print("Unable to retrieve user list from database.")


	def on_chat_message(self,msg):
		content_type, chat_type, chat_id = telepot.glance(msg)

		self.chat_bot.sendChatAction(chat_id, action = "typing")
		self.chat_handler(content_type, chat_type, chat_id, msg)
	

	def chat_handler(self, content_type, chat_type, chat_id, msg):
		if content_type != "text":
			self.chat_bot.sendMessage(chat_id, config["unsupported_feature_text"])
			return

		# Check to see if chat_id is in pending list and process for subscription message.
		if self.is_in_pending_list(chat_id):
			if self.agree_to_subscribe(msg["text"]):
				self.pending_list.remove(chat_id)
				self.active_users.append(chat_id)
				self.set_chat_id(chat_id)
				self.chat_bot.sendMessage(chat_id, self.config["welcome_text"])
				self.send_usage_msg(chat_id)
				return

			else:
				self.chat_bot.sendMessage(chat_id, self.config["acknowledgement_text"])
				return

		# Check to see if user is a first timer.
		if self.is_first_time(msg["text"], chat_id):
			self.chat_bot.sendMessage(chat_id, self.config["greeting_text"])
			self.pending_list.append(chat_id)
			return

		# User is part of active_users. Process message.
		self.process_chat_msg(chat_id, msg["text"].strip().split(), msg["chat"]["first_name"])

	def process_chat_msg(self, chat_id, msg_words, first_name):
		try:
			self.commands[msg_words[0]](chat_id, msg_words, first_name)
		except:
			self.send_usage_msg(chat_id, first_name)	

	def process_status_command(self, chat_id, msg_words, first_name):
		print("status request")

	def process_online_command(self, chat_id, msg_words, first_name):
		if all(device.online == False for device in self.current_devices):
			self.chat_bot.sendMessage(chat_id, "No online device(s).")
			return

		msg_str = "{}, these are the online device(s):\n\n".format(first_name)
		count = 1
		for device in self.current_devices:
			if device.online:
				device_info_str = "[{:2}] {:15}\t{:17}\n".format(count, device.ip, device.hostname)
				msg_str += device_info_str
				count += 1

		self.chat_bot.sendMessage(chat_id,msg_str)

	def process_offline_command(self, chat_id, msg_words, first_name):
		if all(device.online == True for device in self.current_devices):
			self.chat_bot.sendMessage(chat_id, "No offline device(s).")
			return
			
		msg_str = "{}, these are the offline device(s):\n\n".format(first_name)
		count = 1
		for device in self.current_devices:
			if not device.online:
				device_info_str = "[{:2}] {:15}\t{:17}\n".format(count, device.ip, device.hostname)
				msg_str += device_info_str
				count += 1

		self.chat_bot.sendMessage(chat_id,msg_str)

	def send_usage_msg(self, chat_id, first_name):
		msg_str = "Hey {}, This is how you use the bot:\n\n".format(first_name)
		status_command_msg = "*/status* <hostname>\n"
		online_command_msg = "{:8}\t- Show current online devices.\n".format("*/online*")
		offline_command_msg = "{:8}\t- Show current offline devices.\n".format("*/offline*")

		msg_str = msg_str + status_command_msg + online_command_msg + offline_command_msg
		self.chat_bot.sendMessage(chat_id, msg_str, parse_mode = "Markdown")

	def is_in_pending_list(self,chat_id):
		return chat_id in self.pending_list

	def agree_to_subscribe(self,text):
		return text == "/yes"

	def is_first_time(self, text, chat_id):
		return text == "/start" and chat_id not in self.active_users

	def access_database(self):
		return sqlite3.connect(os.path.join(self.config["chatbot_folder"], self.config["database_name"]))

	def set_chat_id(self,chat_id):
		db = self.access_database()
		cursor = db.cursor()

		try:
			cursor.execute('''INSERT INTO {tn} 
			(CHAT_ID) VALUES ({value})
			'''.format(tn = "USERS",value = (chat_id)))
			db.commit()

		except:
			print("Duplicated Chat ID in database: {}".format(chat_id))

		finally:
			db.close()

	def get_chat_ids(self):
		try:
			db = self.access_database()
			cursor = db.cursor()

			cursor.execute("SELECT * FROM {tn}".format(tn="USERS"))
			rows = cursor.fetchall()

			for row in rows:
				if not row[0] in self.active_users: 
					self.active_users.append(int(row[0]))
				else:
					print("User already in list: {}".format(row[0]))

			db.close()
			return True

		except:
			return False

	def send_message_to_all(self, message):
		self.get_chat_ids()

		for chat_id in self.active_users:
			t = Thread(target = self.chat_bot.sendMessage, args = (chat_id, message))
			t.start()
			time.sleep(0.01)

	def handle_delta_payload(self):
		if not (self.last_delta_uuid is None):
			if self.last_delta_uuid is self.delta["id"]:
				return
		
		self.last_delta_uuid = self.delta["id"]

		for device in self.delta["added_devices"]:
			if not (device in self.delta["status_changes"]):
				msg = "{} [{}] is now online!".format(device.ip, device.hostname)
				self.send_message_to_all(msg)

		for device in self.delta["dropped_devices"]:
			if not (device in self.delta["status_changes"]):
				msg = "{} [{}] is now offine!".format(device.ip, device.hostname)
				self.send_message_to_all(msg)

		for device in self.delta["status_changes"]:
			msg = "{ip} [{name}] is now {status}!".format(ip = device.ip, name = device.hostname, status = "online" if (device.online is True) else "offline")
			self.send_message_to_all(msg)

	def get_payload(self):
		while(True):
			payload = self.device_queue.get()

			if payload is None:
				print("[CHATBOT] Sleeping...")
				time.sleep(1)

			else:
				self.current_devices = payload["devices"]
				self.delta = payload["delta"]
				self.handle_delta_payload()

				i = 1
				for device in self.current_devices:
					if self.config["verbose"]: print("[CHATBOT] [{:2}] {:15}\t{:17}\t{:10}\t{}".format(i, device.ip, device.mac, device.hostname, device.online))
					i += 1